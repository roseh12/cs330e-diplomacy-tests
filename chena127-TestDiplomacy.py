#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----
    
    def test_read_1(self):
        s = "A Madrid Hold\n"
        army, location, action, target = diplomacy_read(s)
        self.assertEqual(army, ['A'])
        self.assertEqual(location, ['Madrid'])
        self.assertEqual(action, ['Hold'])
        self.assertEqual(target, [None])
        
    def test_read_2(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n"
        army, location, action, target = diplomacy_read(s)
        self.assertEqual(army, ['A', 'B', 'C'])
        self.assertEqual(location, ['Madrid', 'Barcelona', 'London'])
        self.assertEqual(action, ['Hold', 'Move', 'Support'])
        self.assertEqual(target, [None, 'Madrid', 'B'])
    
    def test_read_3(self):
        s = ""
        army, location, action, target = diplomacy_read(s)
        self.assertEqual(army, [])
        self.assertEqual(location, [])
        self.assertEqual(action, [])
        self.assertEqual(target, [])
        
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval(["A"], ["Madrid"], ["Hold"], [None])
        self.assertEqual(v, ["Madrid"])

    def test_eval_2(self):
        v = diplomacy_eval(["A", "B", "C"], ["Madrid", "Barcelona", "London"], \
        ["Hold", "Move", "Support"], [None, "Madrid", "B"])
        self.assertEqual(v, ["[dead]", "Madrid", "London"])

    def test_eval_3(self):
        v = diplomacy_eval(["A", "B", "C", "D", "E"], ["Madrid", "Barcelona", "London", "Paris", "Austin"], \
        ["Hold", "Move", "Move", "Support", "Support"], [None, "Madrid", "Madrid", "B", "A"])
        self.assertEqual(v, ["[dead]", "[dead]", "[dead]", "Paris", "Austin"])

    def test_eval_4(self):
        v = diplomacy_eval([], [], [], [])
        self.assertEqual(v, [])

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, ["A"], ["Madrid"])
        self.assertEqual(w.getvalue(), "A Madrid\n")
        
    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, ["A", "B", "C"], ["[dead]", "Madrid", "London"])
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")
        
    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, [], [])
        self.assertEqual(w.getvalue(), "")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")
            
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")
            
    def test_solve_3(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
