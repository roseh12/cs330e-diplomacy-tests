from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

class TestDiplomacy (TestCase):
	# ----
	# read
	# ----

	def test_read(self):
		s = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
		i = diplomacy_read(s)
		self.assertEqual(i, ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B"])

	def test_read_2(self):
		s = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
		i = diplomacy_read(s)
		self.assertEqual(i, ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B", "D Austin Move London"])

	def test_read_3(self):
		s = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
		i = diplomacy_read(s)
		self.assertEqual(i, ["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid",  "D Paris Support B", "E Austin Support A"])

	# ----
	# Eval
	# ----

	def test_eval(self):
		v = diplomacy_eval(["A Madrid Hold","B Barcelona Move Madrid","C London Support B"])
		self.assertEqual(v, ["A [dead]","B Madrid","C London"])

	def test_eval_2(self):
		v = diplomacy_eval(["A Madrid Hold","B Barcelona Move Madrid"])
		self.assertEqual(v, ["A [dead]","B [dead]"])

	def test_eval_3(self):
		v = diplomacy_eval(["A Madrid Hold"])
		self.assertEqual(v, ["A Madrid"])

	# -----
	# Print
	# -----

	def test_print(self):
		w = StringIO()
		army_list = ["A [dead]","B Madrid","C London"]
		for i in army_list:
			diplomacy_print(w, i)
		self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")
	
	def test_print_2(self):
		w = StringIO()
		army_list = ["A [dead]","B [dead]"]
		for i in army_list:
			diplomacy_print(w, i)
		self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")


	def test_print_3(self):
		w = StringIO()
		army_list = ["A Madrid"]
		for i in army_list:
			diplomacy_print(w,i)
		self.assertEqual(w.getvalue(), "A Madrid\n")

	# -----
	# Solve
	# -----

	def test_solve(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A [dead]\nB Madrid\nC London\n")

	def test_solve_2(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

	def test_solve_3(self):
		r = StringIO("A Austin Move Houston\nB Houston Move Austin\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A Houston\nB Austin\n")

	def test_solve_4(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

# ----
# main
# ----

if __name__ == "__main__":
	main()


""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
