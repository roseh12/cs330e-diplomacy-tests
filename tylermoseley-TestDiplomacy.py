#!/usr/bin/env python3

from io import StringIO
from unittest import main, TestCase

from Diplomacy import *


class TestDiplomacy (TestCase):

    def test_select_1 (self):
        armies = create_moves_support([{"army": "A", "city": "Madrid", "action": "Hold"},
                                       {"army": "B", "city": "Barcelona", "action": "Move", "action_dep": "Madrid"},
                                       {"army": "C", "city": "London", "action": "Support", "action_dep": "B"},
                                       {"army": "D", "city": "Paris", "action": "Move", "action_dep": "London"}])
        self.assertEqual(list(select(armies, lambda d: False)), [])

    def test_select_2 (self) :
        armies = create_moves_support([{"army": "A", "city": "Madrid", "action": "Hold"},
                                       {"army": "B", "city": "Barcelona", "action": "Move", "action_dep": "Madrid"},
                                       {"army": "C", "city": "London", "action": "Support", "action_dep": "B"},
                                       {"army": "D", "city": "Paris", "action": "Move", "action_dep": "London"}])
        self.assertEqual(next(select(armies, lambda d: d.city == armies["A"].city)).name, "A")
        self.assertEqual(next(select(armies, lambda d: d.city == armies["A"].city)).city, "Madrid")

    def test_battle_1(self):
        armies = create_moves_support([{"army": "A", "city": "Madrid", "action": "Hold"},
                                 {"army": "B", "city": "Barcelona", "action": "Move", "action_dep": "Madrid"},
                                 {"army": "C", "city": "London", "action": "Support", "action_dep": "B"},
                                 {"army": "D", "city": "Paris", "action": "Move", "action_dep": "London"}])
        results = battle(armies, "C")
        self.assertEqual(results, ("B", "C"))
        self.assertEqual(armies["A"].name, "A")
        self.assertEqual(armies["A"].final, "")
        self.assertEqual(armies["B"].name, "B")
        self.assertEqual(armies["B"].final, "")
        self.assertEqual(armies["C"].name, "C")
        self.assertEqual(armies["C"].final, "[dead]")
        self.assertEqual(armies["D"].name, "D")
        self.assertEqual(armies["D"].final, "[dead]")

    def test_battle_2(self):
        armies = create_moves_support([{"army": "A", "city": "Madrid", "action": "Hold"},
                                 {"army": "B", "city": "Barcelona", "action": "Move", "action_dep": "Madrid"},
                                 {"army": "C", "city": "London", "action": "Support", "action_dep": "B"},
                                 {"army": "D", "city": "Paris", "action": "Move", "action_dep": "London"}])
        results = battle(armies, "A")
        self.assertEqual(results, ())
        self.assertEqual(armies["A"].name, "A")
        self.assertEqual(armies["A"].final, "[dead]")
        self.assertEqual(armies["B"].name, "B")
        self.assertEqual(armies["B"].final, "Madrid")
        self.assertEqual(armies["C"].name, "C")
        self.assertEqual(armies["C"].final, "")
        self.assertEqual(armies["D"].name, "D")
        self.assertEqual(armies["D"].final, "")

    def test_battle_3(self):
        armies = create_moves_support([{"army": "A", "city": "Madrid", "action": "Hold"},
                                 {"army": "B", "city": "Barcelona", "action": "Move", "action_dep": "Madrid"},
                                 {"army": "C", "city": "London", "action": "Support", "action_dep": "B"},
                                 {"army": "D", "city": "Paris", "action": "Move", "action_dep": "Madrid"},
                                 {"army": "E", "city": "Austin", "action": "Support", "action_dep": "D"}])
        results = battle(armies, "A")
        self.assertEqual(results, ())
        self.assertEqual(armies["A"].name, "A")
        self.assertEqual(armies["A"].final, "[dead]")
        self.assertEqual(armies["B"].name, "B")
        self.assertEqual(armies["B"].final, "[dead]")
        self.assertEqual(armies["C"].name, "C")
        self.assertEqual(armies["C"].final, "")
        self.assertEqual(armies["D"].name, "D")
        self.assertEqual(armies["D"].final, "[dead]")
        self.assertEqual(armies["E"].name, "E")
        self.assertEqual(armies["E"].final, "")

    def test_battle_4(self):
        armies = create_moves_support([{"army": "A", "city": "Madrid", "action": "Hold"},
                                       {"army": "B", "city": "Barcelona", "action": "Move", "action_dep": "Madrid"},
                                       {"army": "C", "city": "London", "action": "Support", "action_dep": "B"},
                                       {"army": "D", "city": "Paris", "action": "Move", "action_dep": "Madrid"},
                                       {"army": "E", "city": "Austin", "action": "Support", "action_dep": "D"}])
        results = battle(armies, "A")
        self.assertEqual(results, ())
        self.assertEqual(armies["A"].name, "A")
        self.assertEqual(armies["A"].final, "[dead]")
        self.assertEqual(armies["B"].name, "B")
        self.assertEqual(armies["B"].final, "[dead]")
        self.assertEqual(armies["C"].name, "C")
        self.assertEqual(armies["C"].final, "")
        self.assertEqual(armies["D"].name, "D")
        self.assertEqual(armies["D"].final, "[dead]")
        self.assertEqual(armies["E"].name, "E")
        self.assertEqual(armies["E"].final, "")

    def test_create_moves_support_1(self):
        r = create_moves_support([{"army": "A", "city": "Madrid", "action": "Hold"},
                                 {"army": "B", "city": "London", "action": "Move", "action_dep": "Madrid"},
                                 {"army": "C", "city": "Austin", "action": "Support", "action_dep": "B"},
                                 {"army": "D", "city": "Dallas", "action": "Support", "action_dep": "B"}])
        self.assertEqual(r["A"].name, "A")
        self.assertEqual(r["A"].city, "Madrid")
        self.assertEqual(r["A"].support, [])
        self.assertEqual(r["A"].supporting, None)
        self.assertEqual(r["B"].name, "B")
        self.assertEqual(r["B"].city, "Madrid")
        self.assertEqual(r["B"].support, ["C", "D"])
        self.assertEqual(r["B"].supporting, None)
        self.assertEqual(r["C"].name, "C")
        self.assertEqual(r["C"].city, "Austin")
        self.assertEqual(r["C"].support, [])
        self.assertEqual(r["C"].supporting, "B")
        self.assertEqual(r["D"].name, "D")
        self.assertEqual(r["D"].city, "Dallas")
        self.assertEqual(r["D"].support, [])
        self.assertEqual(r["D"].supporting, "B")

    def test_create_moves_support_2(self):
        r = create_moves_support([{"army": "A", "city": "Austin", "action": "Support", "action_dep": "B"},
                                 {"army": "B", "city": "Dallas", "action": "Support", "action_dep": "A"}])
        self.assertEqual(r["A"].name, "A")
        self.assertEqual(r["A"].city, "Austin")
        self.assertEqual(r["A"].support, ["B"])
        self.assertEqual(r["A"].supporting, "B")
        self.assertEqual(r["B"].name, "B")
        self.assertEqual(r["B"].city, "Dallas")
        self.assertEqual(r["B"].support, ["A"])
        self.assertEqual(r["B"].supporting, "A")

    def test_eval_1(self):
        armies = diplomacy_eval([{"army": "A", "city": "Madrid", "action": "Hold"},
                                 {"army": "B", "city": "Barcelona", "action": "Move", "action_dep": "Madrid"},
                                 {"army": "C", "city": "London", "action": "Move", "action_dep": "Madrid"},
                                 {"army": "D", "city": "Paris", "action": "Support", "action_dep": "B"},
                                 {"army": "E", "city": "Austin", "action": "Support", "action_dep": "A"}])
        self.assertEqual(armies["A"].name, "A")
        self.assertEqual(armies["A"].final, "[dead]")
        self.assertEqual(armies["B"].name, "B")
        self.assertEqual(armies["B"].final, "[dead]")
        self.assertEqual(armies["C"].name, "C")
        self.assertEqual(armies["C"].final, "[dead]")
        self.assertEqual(armies["D"].name, "D")
        self.assertEqual(armies["D"].final, "Paris")
        self.assertEqual(armies["E"].name, "E")
        self.assertEqual(armies["E"].final, "Austin")

    def test_eval_2(self):
        armies = diplomacy_eval([{"army": "A", "city": "Madrid", "action": "Hold"},
                                 {"army": "B", "city": "Barcelona", "action": "Move", "action_dep": "Madrid"},
                                 {"army": "C", "city": "London", "action": "Support", "action_dep": "B"},
                                 {"army": "D", "city": "Austin", "action": "Move", "action_dep": "London"}])
        self.assertEqual(armies["A"].name, "A")
        self.assertEqual(armies["A"].final, "[dead]")
        self.assertEqual(armies["B"].name, "B")
        self.assertEqual(armies["B"].final, "[dead]")
        self.assertEqual(armies["C"].name, "C")
        self.assertEqual(armies["C"].final, "[dead]")
        self.assertEqual(armies["D"].name, "D")
        self.assertEqual(armies["D"].final, "[dead]")

    def test_read_1(self):
        s = "A Madrid Hold\n"
        i = diplomacy_read(s)
        self.assertEqual(i, ["A", "Madrid", "Hold"])

    def test_read_2(self):
        s = "B London Move NewYork\n"
        i = diplomacy_read(s)
        self.assertEqual(i, ["B", "London", "Move", "NewYork"])

    def test_read_3(self):
        s = "\n"
        i = diplomacy_read(s)
        self.assertEqual(i, None)

    def test_print_1(self):
        w = StringIO()
        line = Army("A", "Madrid", "")
        line.final = "[dead]"
        diplomacy_print(w, line)
        self.assertEqual(w.getvalue(), "A [dead]\n")

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_2(self):

        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_3(self):
        r = StringIO("A Houston Move Madrid\nB Madrid Support D\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1
$ cat TestDiplomacy.out
$ coverage report -m >> TestDiplomacy.out
"""
