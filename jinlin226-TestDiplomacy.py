from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -------------
# TestDiplomacy
# -------------

class TestDiplomacy(TestCase):
	
	# ----
	# read
	# ----

	def test_read_1(self):
		s = 'A Madrid Hold'
		army, city, action, target = diplomacy_read(s)
		self.assertEqual(army, ['A'])
		self.assertEqual(city, ['Madrid'])
		self.assertEqual(action, ['Hold'])
		self.assertEqual(target, [None])

	def test_read_2(self):
		s = 'A Madrid Hold\nB Barcelona Move Madrid\nC London Support B'
		army, city, action, target = diplomacy_read(s)
		self.assertEqual(army, ['A', 'B', 'C'])
		self.assertEqual(city, ['Madrid', 'Barcelona', 'London'])
		self.assertEqual(action, ['Hold', 'Move', 'Support'])
		self.assertEqual(target, [None, 'Madrid', 'B'])

	def test_read_3(self):
		s = 'A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A'
		army, city, action, target = diplomacy_read(s)
		self.assertEqual(army, ['A', 'B', 'C', 'D', 'E'])
		self.assertEqual(city, ['Madrid', 'Barcelona', 'London', 'Paris', 'Austin'])
		self.assertEqual(action, ['Hold', 'Move', 'Move', 'Support', 'Support'])
		self.assertEqual(target, [None, 'Madrid', 'Madrid', 'B', 'A'])

	def test_read_4(self):
		s = ''
		army, city, action, target = diplomacy_read(s)
		self.assertEqual(army, [])
		self.assertEqual(city, [])
		self.assertEqual(action, [])
		self.assertEqual(target, [])

	# ----
	# eval
	# ----

	def test_eval_1(self):
		x = diplomacy_eval(['A'], ['Madrid'], ['Hold'], [None])
		self.assertEqual(x, ['A Madrid'])

	def test_eval_2(self):
		x = diplomacy_eval(['A', 'B', 'C'], ['Madrid', 'Barcelona', 'London'], ['Hold', 'Move', 'Support'], [None, 'Madrid', 'B'])
		self.assertEqual(x, ['A [dead]', 'B Barcelona', 'C London'])

	def test_eval_3(self):
		x = diplomacy_eval(['A', 'B', 'C', 'D', 'E'], ['Madrid', 'Barcelona', 'London', 'Paris', 'Austin'], ['Hold', 'Move', 'Move', 'Support', 'Support'], [None, 'Madrid', 'Madrid', 'B', 'A'])
		self.assertEqual(x, ['A [dead]', 'B [dead]', 'C [dead]', 'D Paris', 'E Austin'])

	def test_eval_4(self):
		x = diplomacy_eval([],[],[],[])
		self.assertEqual(x, [])

	# -----
	# print
	# -----

	def test_print_1(self):
		w = StringIO()
		diplomacy_print(w, ['A Madrid'])
		self.assertEqual(w.getvalue(), "A Madrid\n")

	def test_print_2(self):
		w = StringIO()
		diplomacy_print(w, ['A [dead]', 'B Madrid', 'C London'])
		self.assertEqual(w.getvalue(), 'A [dead]\nB Madrid\nC London\n')

	def test_print_3(self):
		w = StringIO()
		diplomacy_print(w, ['A [dead]', 'B [dead]', 'C [dead]', 'D Paris', 'E Austin'])
		self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n')

	def test_print_4(self):
		w = StringIO()
		diplomacy_print(w, [])
		self.assertEqual(w.getvalue(), '')

	# -----
	# solve
	# -----

	def test_solve_1(self):
		r = StringIO('A Madrid Hold')
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(w.getvalue(), 'A Madrid\n')

	def test_solve_2(self):
		r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B')
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(w.getvalue(), 'A [dead]\nB Barcelona\nC London\n')

	def test_solve_3(self):
		r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A')
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n')

	def test_solve_4(self):
		r = StringIO('')
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(w.getvalue(), '')

if __name__ == "__main__":
	main()